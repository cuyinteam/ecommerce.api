'''
docstring
'''
from rest_framework import serializers
from products.models import Product


class ProductSerializer(serializers.ModelSerializer):
    '''
    docstring
    '''
    class Meta:
        '''
    docstring
    '''
        model = Product
        fields = ['id', 'name']

