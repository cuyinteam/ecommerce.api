'''
docstring
'''

from rest_framework import viewsets
from products.serializers import ProductSerializer
from products.models import Product

class ProductViewSet(viewsets.ModelViewSet):
    """
    Get all products API 
    """
    queryset  = Product.objects.all()
    serializer_class = ProductSerializer
