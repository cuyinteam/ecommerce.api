'''
docstring
'''
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from products.views import  ProductViewSet

ROUTER = routers.DefaultRouter()
ROUTER.register(r'products', ProductViewSet)

urlpatterns = [
    path('', include(ROUTER.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
    
]
